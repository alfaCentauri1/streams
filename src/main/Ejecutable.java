package main;

import java.util.ArrayList;
import java.util.List;

public class Ejecutable {
    public static void main(String args[]) {
        System.out.println("Programación API Stream con filtros. ");
        List<User> users = new ArrayList<>();
        users.add(new User("Pedro", 23));
        users.add(new User("Maria", 32));
        users.add(new User("Lisa", 39));
        users.add(new User("Lourdes", 35));
        users.add(new User("Bart", 42));
        users.add(new User("Jose", 4));
        int contador = (int) users.stream()
                .filter( user -> user.getAge() > 18 )
                .filter( user -> user.getName().startsWith("L"))
                .count();
        System.out.println("Cantidad: " + contador);
        //
        System.out.println("Fin. ");
    }
}
